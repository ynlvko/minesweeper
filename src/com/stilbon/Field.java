package com.stilbon;

public class Field {
    private int[][]  field;
    private String[] fieldDescription;

    public Field(String fieldString) {
        fieldDescription = fieldString.split("\\s");

        field = new int[3][3];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                check(i, j);
            }
        }
    }

    public void show() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (isBomb(i, j)) {
                    System.out.print('*');
                } else {
                    System.out.print(field[i][j]);
                }
            }
            System.out.println();
        }
    }

    private void check(int i, int j) {
        if (isBomb(i, j)) {
            return;
        }
        checkTopLeft(i, j);
        checkTopCenter(i, j);
        checkTopRight(i, j);
        checkCenterLeft(i, j);
        checkCenterRight(i, j);
        checkBottomLeft(i, j);
        checkBottomCenter(i, j);
        checkBottomRight(i, j);
    }

    private boolean isBomb(int i, int j) {
        return fieldDescription[i].charAt(j) == '*';
    }

    private void checkTopLeft(int i, int j) {
        if (i > 0 && j > 0 && isBomb(i - 1, j - 1)) {
            field[i][j]++;
        }
    }

    private void checkTopCenter(int i, int j) {
        if (i > 0 && isBomb(i - 1, j)) {
            field[i][j]++;
        }
    }

    private void checkTopRight(int i, int j) {
        if (i > 0 && j < field[i - 1].length - 1 && isBomb(i - 1, j + 1)) {
            field[i][j]++;
        }
    }

    private void checkCenterLeft(int i, int j) {
        if (j > 0 && isBomb(i, j - 1)) {
            field[i][j]++;
        }
    }

    private void checkCenterRight(int i, int j) {
        if (j < field[i].length - 1 && isBomb(i, j + 1)) {
            field[i][j]++;
        }
    }

    private void checkBottomLeft(int i, int j) {
        if (i < field.length - 1 && j > 0 && isBomb(i + 1, j - 1)) {
            field[i][j]++;
        }
    }

    private void checkBottomCenter(int i, int j) {
        if (i < field.length - 1 && isBomb(i + 1, j)) {
            field[i][j]++;
        }
    }

    private void checkBottomRight(int i, int j) {
        if (i < field.length - 1 && j < field[i].length - 1 && isBomb(i + 1, j + 1)) {
            field[i][j]++;
        }
    }
}
